///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Trevor Chang <@trevorkw>
// @date  Feb 21 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

//convert to seconds
#define YEAR_S 31557600
#define DAY_S 86400
#define HOUR_S 3600
#define MIN_S 60

int main() {


   //Declare current time
   time_t now;
   //Declare time variables
   int years, days, hours, mins, secs, count;

   //Declare and fixed date
   struct tm start;
   start = *localtime(&now);

   start.tm_sec = 0;
   start.tm_min = 0;
   start.tm_hour = 16;
   start.tm_mday = 20;
   start.tm_mon = 1;
   start.tm_year = 122;
   
   printf("Reference Time: ", localtime(&now));    //can't get it to print in the correct format, use strftime?
while(1){
      //initialie current and fixed time
      time(&now);
      mktime(&start);

      count = difftime(now, mktime(&start));

      years = count / YEAR_S;
      days = count % YEAR_S / DAY_S;
      hours = count % DAY_S / HOUR_S;
      mins = count % HOUR_S / MIN_S;
      secs = count % MIN_S;

      printf("Years: %i  Days: %i  Hours: %i  Minutes: %i  Seconds: %i\n", years, days, hours, mins, secs);
      sleep(1);
   }

   return 0;
}
